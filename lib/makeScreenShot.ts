import getTimeString from "./getTimeString";

var i: number = 0;
const startTime = new Date();
const time = startTime.getTime();

const timestring = getTimeString();

async function captureOutPut() {}

async function makescreenShot({
  page,
  browserName,
  browser,
  testName,
  title,
  printDom = false,
}) {
  i++;
  //   const version = browser ? browser.version() : "x";
  const diff = time - new Date().getTime();

  const path = `./snapshots/${testName}/${browserName}/${timestring}/${i}-${diff}${
    title ? "-" + title : ""
  }.png`;

  //   console.log(startTime.getTime(), path);
  //   if (printDom) {
  //     console.log("Printing Dom Content");
  //     page.content().then((x) => console.log(x));
  //   }
  console.log(path);

  await page.screenshot({ path });
}

export default makescreenShot;
