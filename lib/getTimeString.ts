const getTimeString = () => {
  const now = new Date();
  const year = now.getFullYear();
  const month = now.getMonth() + 1;
  const date = now.getDate() + 1;
  const hours = now.getHours();
  const minutes = now.getMinutes();

  return (
    year +
    "-" +
    (month < 10 ? "0" + month : month) +
    "-" +
    (date < 10 ? "0" + date : date) +
    "_" +
    (hours < 10 ? "0" + hours : hours) +
    "." +
    (minutes < 10 ? "0" + minutes : minutes)
  );
};

export default getTimeString