const random = Math.floor(Math.random() * 10000);

const participants = [
  { EMAIL: `theocat${random}@mailinator.com`, FIRST: "theo", LAST: "cat" },
  {
    EMAIL: `rocky${random}@mailinator.com`,
    FIRST: "rocky",
    LAST: "dog",
  },
];

export default participants;
