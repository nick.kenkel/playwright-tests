import { test, expect } from "@playwright/test";
import { participants, user } from "../mockdata";
import makescreenShot from "../../lib/makeScreenShot";
import getTimeString from "../../lib/getTimeString";

import { chromium } from "@playwright/test";

const URL = process.env.URL;
const EMAIL = participants[0].EMAIL;
const FIRST = participants[0].FIRST;
const LAST = participants[0].LAST;

const testName = "ESIGN ORDER";

test(testName, async ({ browserName, browser }) => {
  // const browser = await chromium.launch({ headless: true });

  const context = await browser.newContext({
    recordVideo: {
      dir: `./videos/${browserName}/${getTimeString()}`,
      size: { width: 640, height: 480 },
    },
  });

  const page = await context.newPage();

  const props = { page, browserName, browser, testName };

  // const browsercontext = await browser.contexts()[0];

  // console.log(Object.keys(browsercontext));
  // console.log(Object.keys(browsercontext["_options"]));
  // console.log(browsercontext["_options"].isMobile);
  // console.log(await browser.contexts()[0].options);

  await page.goto(`${URL}/portal`);
  await makescreenShot({ ...props, title: "portal" });

  await page.locator('input[name="name"]').click();
  await makescreenShot({ ...props, title: "Organization Name" });

  await page.locator('button:has-text("Submit")').click();
  await makescreenShot({ ...props, title: "CLICK SUBMIT" });

  await page.locator('button:has-text("Continue with eNotaryLog")').click();
  await makescreenShot({ ...props, title: "Continue with eNotaryLog" });

  //Webkit is timing out on email

  //ENTER EMAIL

  await makescreenShot({ ...props, printDom: true, title: "PRE EMAIL" });

  await page
    .locator('[placeholder="Email\\,\\ phone\\,\\ or\\ Skype"]')
    .click();
  await makescreenShot({ ...props, printDom: true, title: "Click Into FIeld" });

  await page
    .locator('[placeholder="Email\\,\\ phone\\,\\ or\\ Skype"]')
    .fill(user.EMAIL);
  await makescreenShot({ ...props, printDom: true, title: "enter email" });

  // await page.locator('[type="email"]').click();
  // await page.locator('[type="email"]').fill(user.EMAIL);
  await page.locator("text=Next").click();
  await makescreenShot({ ...props, printDom: true, title: "ENTER EMAIL" });

  //ENTER PASSWORD
  await page.locator('[placeholder="Password"]').click();
  await page.locator('[placeholder="Password"]').fill(user.PASSWORD);
  //await makescreenShot({...props,title:"ENTER PASSWORD"});

  //CLICK SIGN IN BUTTON
  await page.locator("text=Sign in").click();
  //await makescreenShot({...props,title:"ENTER PASSWORD"});

  //DEVICE SPECIFIC
  //

  // Click text=Yes
  // REMEMBER INFO

  // THIS IS TRIPPING UP MOBILE CHROMIUM
  await Promise.all([
    page.waitForNavigation(/*{ url: 'http://localhost:4044/app/reports/dashboard' }*/),
    page.locator("text=Yes").click(),
  ]);

  //CLICK ORDERS

  // await page.goto(`${URL}/app/reports/dashboard`);
  // await makescreenShot({...props,title:"TIMEOUT 85"});

  await page.waitForNavigation(/*{ url: 'http://localhost:4044/app/reports/dashboard' }*/),
    // await makescreenShot({...props,title:"TIMEOUT 88"});

    // await makescreenShot({...props,title:"TIMEOUT 91"});

    await page.locator('a[role="button"]:has-text("Orders")').click();
  // await makescreenShot({...props,title:"TIMEOUT 94"});

  await expect(page).toHaveURL("http://localhost:4044/app/orders");
  await makescreenShot({ ...props, title: "Orders" });

  //CLICK CREATE ESIGN
  await page.locator('a[role="button"]:has-text("eSign Order")').click();
  await expect(page).toHaveURL("http://localhost:4044/app/orders/create/esign");
  await makescreenShot({ ...props, title: "CREATE ESIGN" });

  //ADD PARTICIPANT(s)
  await page.locator('[data-testid="add-participant-button"]').click();
  await makescreenShot({ ...props, title: "ADD PARTICIPANT" });
  //ENTER FIRST NAME
  await page.locator('input[name="firstName"]').click();
  await page.locator('input[name="firstName"]').fill(FIRST);
  await makescreenShot({ ...props, title: "ENTER FIRST NAME" });
  //ENTER LAST NAME
  await page.locator('input[name="lastName"]').click();
  await page.locator('input[name="lastName"]').fill(LAST);
  await makescreenShot({ ...props, title: "ENTER LAST NAME" });
  //ENTER EMAIL
  await page.locator('input[name="email"]').click();
  await page.locator('input[name="email"]').fill(EMAIL);
  await makescreenShot({ ...props, title: "ENTER EMAIL" });
  //SET NO AUTHENTICATION
  await page.locator('div[role="button"]:has-text("​")').click();
  await page.locator("text=No Authentication").click();
  await page
    .locator(
      'text=Participant InformationParticipant type *First name *Middle nameLast name *Email >> [data-testid="submit-button"]'
    )
    .click();
  await makescreenShot({ ...props, title: "NO AUTHENTICATION" });
  //SAVE PARTICIPANT
  await page
    .locator(
      'div[role="button"]:has-text("Transaction Add-onsSet Scheduling, Signer Order, Tagging Queue, etc")'
    )
    .click();
  await makescreenShot({ ...props, title: "SAVE PARTICIPANT" });

  //ENABLE SIGNING ORDER (OPTIONAL?)
  // await page.locator('input[name="enableSigningOrder"]').check();
  // await page.locator('[data-testid="\\31 "] button:has-text("Next")').click();
  // await makescreenShot({...props,title:"ENABLE SIGNING ORDER"});

  //NEXT ACCORDIAN
  await page.locator('[data-testid="\\31 "] button:has-text("Next")').click();
  await makescreenShot({ ...props, title: "NEXT testid-31" });

  //CLICK BROWSE
  await makescreenShot({ ...props, title: "CLICK BROWSE" });

  //UPLOAD DOCUMENT
  await page
    .locator('[type="file"]')
    .first()
    .setInputFiles("./mockdata/pdfs/two-page.pdf");
  await makescreenShot({ ...props, title: "UPLOAD DOCUMENT" });

  //ADD SIGNATURE
  await page.frameLocator("#webviewer-1").locator("img").first().click();
  await page
    .frameLocator("#webviewer-1")
    .locator("#pageWidgetContainer0")
    .click();
  await makescreenShot({ ...props, title: "ADD SIGNATURE TAG" });

  await page.frameLocator("#webviewer-1").locator("img").nth(1).click();
  await page
    .frameLocator("#webviewer-1")
    .locator("#pageWidgetContainer1")
    .click();
  await makescreenShot({ ...props, title: "ADD INITIAL TAG" });

  //SAVE DOCUMENT
  await page.locator('[data-testid="save-document-button"]').click();
  await makescreenShot({ ...props, title: "SAVE DOCUMENT" });

  //NEXT BUTTON
  await page
    .locator('[data-testid="\\32 "] [data-testid="next-button"]')
    .click();
  await makescreenShot({ ...props, title: "NEXT testid-32" });

  //SET ORDER
  await page.locator("text=--").click();
  await makescreenShot({ ...props, title: "SET ORDER" });
  await page.locator('li[role="option"]:has-text("1")').click();
  await makescreenShot({ ...props, title: "SET 1" });

  //NEXT BUTTON
  await page.locator('[data-testid="\\33 "] button:has-text("Next")').click();
  await makescreenShot({ ...props, title: "NEXT testid-33" });

  //SUBMIT ORDER
  await page.locator('div[role="dialog"] button:has-text("Submit")').click();
  await makescreenShot({ ...props, title: "SUBMIT ESIGNATURE ORDER" });

  //ORDER SAVED TOAST
  await page.locator('div[role="alert"]:has-text("Order Saved!")').click();
  await makescreenShot({ ...props, title: "ORDER SAVED" });

  //MAILINATOR

  const mailinatorpath = `https://www.mailinator.com/v4/public/inboxes.jsp?to=${
    EMAIL.split("@")[0]
  }`;
  console.log(mailinatorpath);

  await page.goto(mailinatorpath);
  await makescreenShot({ ...props, title: "MAILINATOR" });

  //CLICK EMAIL
  await page.locator("text=eNotaryLog eSignature Request").first().click();
  await makescreenShot({ ...props, title: "CLICK SIGNATURE REQUEST" });
  //CLICK CONTINUE BUTTON

  const DOCUMENTURL = await page
    .frameLocator("text=<!-- HTML EMAIL BODY -->")
    .locator('a:has-text("CONTINUE")')
    .getAttribute("href");
  console.log(DOCUMENTURL);
  // console.log(await  page.frameLocator('text=<!-- HTML EMAIL BODY -->').locator('a:has-text("CONTINUE")').nth(1).getAttribute("href"))
  await page
    .frameLocator("text=<!-- HTML EMAIL BODY -->")
    .locator('a:has-text("CONTINUE")')
    .click();
  await makescreenShot({ ...props, title: "CLICK CONTINUE BUTTON" });

  const [page1] = await Promise.all([
    page.waitForEvent("popup"),
    page
      .frameLocator("text=<!-- HTML EMAIL BODY -->")
      .locator('a:has-text("CONTINUE")')
      .click(),
  ]);

  // await page.goto(DOCUMENTURL);
  // await makescreenShot({ ...props, title: "DOCUMENTURL" });

  props.page = page1;

  // Check input[type="checkbox"] >> nth=0
  await page1.locator('input[type="checkbox"]').first().check();
  await makescreenShot({ ...props, title: "FIRST CHECK BOX" });

  // Check input[type="checkbox"] >> nth=1
  await page1.locator('input[type="checkbox"]').nth(1).check();
  await makescreenShot({ ...props, title: "SECOND CHECK BOX" });

  // Click button:has-text("Continue")
  await page1.locator('button:has-text("Continue")').click();
  await makescreenShot({ ...props, title: "CLICK CONTINUE" });

  // Click [data-testid="sigInitStepper-Set\ your\ Signature"]
  await page1
    .locator('[data-testid="sigInitStepper-Set\\ your\\ Signature"]')
    .click();
  await makescreenShot({ ...props, title: "SET SIGNATURE" });

  await page1.locator('[data-testid="nextBtn"]').click();
  await makescreenShot({ ...props, title: "NEXT BUTTON" });

  // Click [data-testid="nextBtn"]
  await page1.locator('[data-testid="nextBtn"]').click();
  await makescreenShot({ ...props, title: "NEXT BUTTON" });

  // await page1.locator('button:has-text("Get Started")').click();
  // await makescreenShot({ ...props, title: "GET STARTED CLICK" });

  // // Click [data-testid="autoScrollDisclaimerBtn"]
  // await page1.locator('[data-testid="autoScrollDisclaimerBtn"]').click();
  // await makescreenShot({ ...props, title: "AUTO SCROLL" });

  // await page1.locator('button:has-text("Stop Autoscroll")').click();

  //TODO ==> Loop until all elements are signed

  // await page1.frameLocator("#webviewer-1").locator("text=Sign Here￪").click();
  // await makescreenShot({ ...props, title: "SIGN HERE" });

  // //INITIAL HERE
  // await page1
  //   .frameLocator("#webviewer-1")
  //   .locator("text=Initial Here￪")
  //   .click();
  // await makescreenShot({ ...props, title: "INITIAL HERE" });

  // Click button:has-text("Complete")
  //await expect(page.locator('button:has-text("Complete")'));

  // Click #pageWidgetContainer0
  await page1
    .frameLocator("#webviewer-1")
    .locator("#pageWidgetContainer0")
    .click();
  await makescreenShot({ ...props, title: "PAGE ONE Signature" });
  await page1
    .frameLocator("#webviewer-1")
    .locator("#pageWidgetContainer1")
    .click();

  await makescreenShot({ ...props, title: "PAGE TWO Initial" });

  // Click button:has-text("Complete")
  // await page1.locator('button:has-text("Complete")').click();

  await page1.locator('button:has-text("Complete")').click();
  await makescreenShot({ ...props, title: "COMPLETE" });

  await page1
    .locator("text=You have completed all documents for this eSign session.")
    .click({
      clickCount: 3,
    });
  await makescreenShot({ ...props, title: "SUCCESS TOAST" });

  await page1.locator('img[alt="Transaction\\ complete"]').click();
  await makescreenShot({ ...props, title: "TRANSACTION COMPLETE" });
  // await makescreenShot({ ...props, title: "EXTRA SCREEN SHOT" });
  // await makescreenShot({ ...props, title: "FINAL SCREEN SHOT" });

  // await context.close();

  // You have completed all documents... capture

  // Go to http://localhost:3032/session/complete
  // await page.goto('http://localhost:3032/session/complete');
  // // Click img[alt="Transaction\ complete"]
  // await page.locator('img[alt="Transaction\\ complete"]').click();
});
