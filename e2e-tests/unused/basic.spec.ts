import {test, expect, PageScreenshotOptions} from "@playwright/test"


test.describe('sign in', ()=> {
    test.beforeEach(async ({page})=>{
        await page.goto('http://localhost:4044')

    })

    test('enter organization', async({page})=>{
        await expect(page).toHaveURL('http://localhost:4044/portal')
        await page.screenshot({path:"before.png", fullPage:true})
        await page.locator('input').fill("e");
        await expect(page.locator('input')).toHaveValue("e")
        await page.locator('button').click()

        await page.screenshot({path:"after.png", fullPage:true})
        
    }
    )




})